import {
  Count,
  CountSchema,
  Filter,
  repository,
  Where,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getFilterSchemaFor,
  getWhereSchemaFor,
  patch,
  put,
  del,
  requestBody,
} from '@loopback/rest';
import { InvoiceNumber } from '../models';
import { InvoiceNumberRepository } from '../repositories';

export class InvoiceNumberController {
  constructor(
    @repository(InvoiceNumberRepository)
    public invoiceNumberRepository: InvoiceNumberRepository,
  ) { }

  @post('/invoice-numbers', {
    responses: {
      '200': {
        description: 'InvoiceNumber model instance',
        content: { 'application/json': { schema: { 'x-ts-type': InvoiceNumber } } },
      },
    },
  })
  async create(@requestBody() invoiceNumber: InvoiceNumber): Promise<InvoiceNumber> {
    console.log("invoiceNumber: ", invoiceNumber);
    return await this.invoiceNumberRepository.create(invoiceNumber);
  }

  @get('/invoice-numbers/count', {
    responses: {
      '200': {
        description: 'InvoiceNumber model count',
        content: { 'application/json': { schema: CountSchema } },
      },
    },
  })
  async count(
    @param.query.object('where', getWhereSchemaFor(InvoiceNumber)) where?: any,
  ): Promise<Count> {
    return await this.invoiceNumberRepository.count(where);
  }

  @get('/invoice-numbers', {
    responses: {
      '200': {
        description: 'Array of InvoiceNumber model instances',
        content: {
          'application/json': {
            schema: { type: 'array', items: { 'x-ts-type': InvoiceNumber } },
          },
        },
      },
    },
  })
  async find(
    @param.query.object('filter', getFilterSchemaFor(InvoiceNumber)) filter?: any,
  ): Promise<InvoiceNumber[]> {
    return await this.invoiceNumberRepository.find(filter);
  }

  @patch('/invoice-numbers', {
    responses: {
      '200': {
        description: 'InvoiceNumber PATCH success count',
        content: { 'application/json': { schema: CountSchema } },
      },
    },
  })
  async updateAll(
    @requestBody() invoiceNumber: InvoiceNumber,
    @param.query.object('where', getWhereSchemaFor(InvoiceNumber)) where?: any,
  ): Promise<Count> {
    return await this.invoiceNumberRepository.updateAll(invoiceNumber, where);
  }

  @get('/invoice-numbers/{id}', {
    responses: {
      '200': {
        description: 'InvoiceNumber model instance',
        content: { 'application/json': { schema: { 'x-ts-type': InvoiceNumber } } },
      },
    },
  })
  async findById(@param.path.number('id') id: number): Promise<InvoiceNumber> {
    return await this.invoiceNumberRepository.findById(id);
  }

  @patch('/invoice-numbers/{id}', {
    responses: {
      '204': {
        description: 'InvoiceNumber PATCH success',
      },
    },
  })
  async updateById(
    @param.path.number('id') id: number,
    @requestBody() invoiceNumber: InvoiceNumber,
  ): Promise<void> {
    await this.invoiceNumberRepository.updateById(id, invoiceNumber);
  }

  @put('/invoice-numbers/{id}', {
    responses: {
      '204': {
        description: 'InvoiceNumber PUT success',
      },
    },
  })
  async replaceById(
    @param.path.number('id') id: number,
    @requestBody() invoiceNumber: InvoiceNumber,
  ): Promise<void> {
    await this.invoiceNumberRepository.replaceById(id, invoiceNumber);
  }

  @del('/invoice-numbers/{id}', {
    responses: {
      '204': {
        description: 'InvoiceNumber DELETE success',
      },
    },
  })
  async deleteById(@param.path.number('id') id: number): Promise<void> {
    await this.invoiceNumberRepository.deleteById(id);
  }
}
