import {
  Count,
  CountSchema,
  Filter,
  repository,
  Where,
} from '@loopback/repository';

var fs = require('fs');
const XlsxPopulate = require('xlsx-populate');

import {
  post,
  param,
  get,
  getFilterSchemaFor,
  getWhereSchemaFor,
  patch,
  put,
  del,
  requestBody,
} from '@loopback/rest';
import { Orderdata } from '../models';
import { OrderdataRepository } from '../repositories';

import { Utility } from '../utils';

export class OrderdataController {
  constructor(
    @repository(OrderdataRepository)
    public orderdataRepository: OrderdataRepository,
  ) { }

  @post('/orderdata', {
    responses: {
      '200': {
        description: 'Orderdata model instance',
        content: { 'application/json': { schema: { 'x-ts-type': Orderdata } } },
      },
    },
  })
  async create(@requestBody() orderdata: Orderdata): Promise<Orderdata> {
    console.log("orderdata", orderdata);
    console.log("orderdata product", orderdata.produclist);

    //Utility.writeXlsx(orderdata);
    Utility.writeBollaXlsxFromTemplate(orderdata);
    Utility.writeFatturaXlsxFromTemplate(orderdata);
    return await this.orderdataRepository.create(orderdata);
  }

  @get('/orderdata/count', {
    responses: {
      '200': {
        description: 'Orderdata model count',
        content: { 'application/json': { schema: CountSchema } },
      },
    },
  })
  async count(
    @param.query.object('where', getWhereSchemaFor(Orderdata)) where?: any,
  ): Promise<Count> {
    return await this.orderdataRepository.count(where);
  }

  @get('/orderdata', {
    responses: {
      '200': {
        description: 'Array of Orderdata model instances',
        content: {
          'application/json': {
            schema: { type: 'array', items: { 'x-ts-type': Orderdata } },
          },
        },
      },
    },
  })
  async find(
    @param.query.object('filter', getFilterSchemaFor(Orderdata)) filter?: any,
  ): Promise<Orderdata[]> {
    return await this.orderdataRepository.find(filter);
  }

  @patch('/orderdata', {
    responses: {
      '200': {
        description: 'Orderdata PATCH success count',
        content: { 'application/json': { schema: CountSchema } },
      },
    },
  })
  async updateAll(
    @requestBody() orderdata: Orderdata,
    @param.query.object('where', getWhereSchemaFor(Orderdata)) where?: any,
  ): Promise<Count> {
    return await this.orderdataRepository.updateAll(orderdata, where);
  }

  @get('/orderdata/{id}', {
    responses: {
      '200': {
        description: 'Orderdata model instance',
        content: { 'application/json': { schema: { 'x-ts-type': Orderdata } } },
      },
    },
  })
  async findById(@param.path.number('id') id: number): Promise<Orderdata> {
    return await this.orderdataRepository.findById(id);
  }

  @patch('/orderdata/{id}', {
    responses: {
      '204': {
        description: 'Orderdata PATCH success',
      },
    },
  })
  async updateById(
    @param.path.number('id') id: number,
    @requestBody() orderdata: Orderdata,
  ): Promise<void> {
    await this.orderdataRepository.updateById(id, orderdata);
  }

  @put('/orderdata/{id}', {
    responses: {
      '204': {
        description: 'Orderdata PUT success',
      },
    },
  })
  async replaceById(
    @param.path.number('id') id: number,
    @requestBody() orderdata: Orderdata,
  ): Promise<void> {
    await this.orderdataRepository.replaceById(id, orderdata);
  }

  @del('/orderdata/{id}', {
    responses: {
      '204': {
        description: 'Orderdata DELETE success',
      },
    },
  })
  async deleteById(@param.path.number('id') id: number): Promise<void> {
    await this.orderdataRepository.deleteById(id);
  }


}
