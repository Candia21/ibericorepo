import {inject} from '@loopback/core';
import {juggler} from '@loopback/repository';
import * as config from './orderdata.datasource.json';

export class OrderdataDataSource extends juggler.DataSource {
  static dataSourceName = 'orderdata';

  constructor(
    @inject('datasources.config.orderdata', {optional: true})
    dsConfig: object = config,
  ) {
    super(dsConfig);
  }
}
