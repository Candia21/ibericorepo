import {inject} from '@loopback/core';
import {juggler} from '@loopback/repository';
import * as config from './invoice.datasource.json';

export class InvoiceDataSource extends juggler.DataSource {
  static dataSourceName = 'invoice';

  constructor(
    @inject('datasources.config.invoice', {optional: true})
    dsConfig: object = config,
  ) {
    super(dsConfig);
  }
}
