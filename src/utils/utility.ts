'use strict';

import { AnyType } from "@loopback/repository";
var Excel = require('exceljs');
var fs = require('file-system');
const path = './customer/';
const templateBolla = './bolla.xlsx';
const templateFattura = './fattura.xlsx';
const productFile = './Iberico prezzi globale.xlsx';


export class Utility {

    static async writeXlsx(data: any) {
        var workbook = new Excel.Workbook();
        const worksheet = workbook.addWorksheet('Order');
        //console.log('data: ', data);
        worksheet.columns = [
            { header: 'Nome', key: 'name', width: 32 },
            { header: 'Prezzo', key: 'price', width: 32 },
            { header: 'Quantità', key: 'qty', width: 32 },
            { header: 'Peso (in Kg)', key: 'weight', width: 32 }

        ];
        worksheet.getRow(1).font = { family: 4, size: 12, bold: true };
        worksheet.addRow({ name: '', price: '', qty: '', weight: '' });
        var tot: number = 0;
        for (let d of data.produclist) {
            if (d["qty"] > 0) {
                if (d["withWeight"] == true) {
                    tot = tot + parseFloat(d["price"]) * parseInt(d["weight"]);
                    console.log('con peso: ', parseFloat(d["price"]) * parseInt(d["weight"]));
                } else {
                    tot = tot + parseFloat(d["price"]) * parseInt(d["qty"]);
                    console.log('senza peso: ', parseFloat(d["price"]) * parseInt(d["weight"]));
                }
                worksheet.addRow({ name: d["name"], price: d["price"] + ' €', qty: d["qty"], weight: d["weight"] });
            }
        }

        worksheet.getColumn(1).alignment = { vertical: 'top', horizontal: 'center', wrapText: 'true', indent: '1' };
        worksheet.getColumn(2).alignment = { vertical: 'top', horizontal: 'center', wrapText: 'true', indent: '1' };
        worksheet.getColumn(3).alignment = { vertical: 'top', horizontal: 'center', wrapText: 'true', indent: '1' };
        worksheet.getColumn(4).alignment = { vertical: 'top', horizontal: 'center', wrapText: 'true', indent: '1' };



        var cell = worksheet.getCell('F2');
        cell.value = 'Totale';
        var cell = worksheet.getCell('G2');
        console.log('finale: ', tot);
        cell.value = tot + ' €';

        worksheet.getCell('G2').font = { family: 4, size: 10, bold: true };


        const now = new Date();
        await workbook.xlsx.writeFile(path + '/' + data.namecustomer[0].replace(/[^a-zA-Z0-9]/g, '') + '/' + now.getFullYear() + '-' + now.getMonth() + '-' + now.getDate() + '-' + now.getHours() + now.getMinutes() + now.getSeconds() + '.xlsx');
    }

    static async writeXlsxDetails(data: any) {
        var workbook = new Excel.Workbook();
        const worksheet = workbook.addWorksheet('CustomerDetail');
        console.log('data: ', data);
        worksheet.columns = [
            { header: 'Id', key: 'id', width: 10 },
            { header: 'Nome', key: 'name', width: 32 },
            { header: 'P. Iva', key: 'piva', width: 32 },
            { header: 'Telefono', key: 'phone', width: 32 },
            { header: 'E-mail', key: 'email', width: 32 },
            { header: 'Pec', key: 'pec', width: 32 }
        ];

        worksheet.addRow({ id: data["id"], name: data["name"], address: data["address"], piva: data["piva"], phone: data["phone"], email: data["email"], pec: data["pec"] });
        console.log('percorso file dettagli: ', path);
        await workbook.xlsx.writeFile(path + '/' + data.name.replace(/[^a-zA-Z0-9]/g, '') + '/' + data.name.replace(/[^a-zA-Z0-9]/g, '') + '.xlsx');

    }

    static async writeBollaXlsxFromTemplate(data: any) {
        var workbook = new Excel.Workbook();
        console.log('data: ', data);
        const imageId = workbook.addImage({
            buffer: fs.readFileSync('./images/image001.png'),
            extension: 'png',
        });
        workbook.xlsx.readFile(templateBolla).then(function () {
            const now = new Date();
            let day = now.getDate();
            let month = now.getMonth() + 1;
            let year = now.getFullYear();
            let date = '' + day + - + month + - + year;

            // use workbook
            const worksheet = workbook.getWorksheet('Foglio1');
            worksheet.addImage(imageId, 'B1:D4');

            worksheet.getRow(7).getCell(1).value = data.invoiceNumber;
            worksheet.getRow(7).getCell(1).alignment = { vertical: 'middle', horizontal: 'center' };
            worksheet.getRow(7).getCell(2).value = date;
            worksheet.getRow(7).getCell(2).alignment = { vertical: 'middle', horizontal: 'center' };
            worksheet.getRow(7).getCell(3).value = data.customerdetail[0].name;
            worksheet.getRow(7).getCell(3).alignment = { vertical: 'middle', horizontal: 'center' };
            worksheet.getRow(7).getCell(4).value = data.customerdetail[0].piva;
            worksheet.getRow(7).getCell(4).alignment = { vertical: 'middle', horizontal: 'center' };
            for (let i = 0; i < data.produclist.length; i++) {
                if (i == 0) {
                    worksheet.getRow(10).getCell(1).value = data.produclist[i].qty;
                    worksheet.getRow(10).getCell(1).alignment = { vertical: 'middle', horizontal: 'center' };
                    worksheet.getRow(10).getCell(2).value = data.produclist[i].name;
                    worksheet.getRow(10).getCell(2).alignment = { vertical: 'middle', horizontal: 'center' };
                    worksheet.getRow(10).getCell(4).value = '';
                    worksheet.getRow(10).getCell(4).alignment = { vertical: 'middle', horizontal: 'center' };

                }
                else {
                    worksheet.spliceRows(10 + i, 1, [data.produclist[i].qty, data.produclist[i].name, '', '']);
                    let cont = 10 + i;
                    worksheet.mergeCells('B' + cont, 'C' + cont);
                    worksheet.mergeCells('D' + cont, 'E' + cont);
                    worksheet.getRow(10 + i).getCell(1).border = {
                        bottom: { style: 'thin' },
                        right: { style: 'thin' }
                    };
                    worksheet.getRow(10 + i).getCell(1).alignment = { vertical: 'middle', horizontal: 'center' };

                    worksheet.getRow(10 + i).getCell(2).border = {
                        bottom: { style: 'thin' },
                        right: { style: 'thin' }
                    };
                    worksheet.getRow(10 + i).getCell(2).alignment = { vertical: 'middle', horizontal: 'center' };

                    worksheet.getRow(10 + i).getCell(4).border = {
                        bottom: { style: 'thin' },
                        right: { style: 'thin' }
                    };
                    worksheet.getRow(10 + i).getCell(4).alignment = { vertical: 'middle', horizontal: 'center' };
                }
            }
            console.log('worksheetFinale: ', worksheet.getRow(10).getCell(2).value);
            let nameFile = 'Bolla' + now.getFullYear() + '-' + now.getMonth() + '-' + now.getDate() + '-' + now.getHours() + now.getMinutes() + now.getSeconds();
            console.log('percorso finale: ', path + nameFile + '.xlsx');

            workbook.xlsx.writeFile('bolle/' + data.namecustomer[0].replace(/[^a-zA-Z0-9]/g, '') + nameFile + '.xlsx');

        });
    }

    static async writeFatturaXlsxFromTemplate(data: any) {
        var workbook = new Excel.Workbook();
        console.log('data: ', data);
        const imageId = workbook.addImage({
            buffer: fs.readFileSync('./images/image001.png'),
            extension: 'png',
        });
        workbook.xlsx.readFile(templateFattura).then(function () {
            const now = new Date();
            let day = now.getDate();
            let month = now.getMonth() + 1;
            let year = now.getFullYear();
            let date = '' + day + - + month + - + year;

            // use workbook
            const worksheet = workbook.getWorksheet('Foglio1');
            worksheet.addImage(imageId, 'B1:D4');

            worksheet.getRow(7).getCell(1).value = data.invoiceNumber;
            worksheet.getRow(7).getCell(1).alignment = { vertical: 'middle', horizontal: 'center' };
            worksheet.getRow(7).getCell(2).value = date;
            worksheet.getRow(7).getCell(2).alignment = { vertical: 'middle', horizontal: 'center' };
            worksheet.getRow(7).getCell(3).value = data.customerdetail[0].name;
            worksheet.getRow(7).getCell(3).alignment = { vertical: 'middle', horizontal: 'center' };
            worksheet.getRow(7).getCell(4).value = data.customerdetail[0].piva;
            worksheet.getRow(7).getCell(4).alignment = { vertical: 'middle', horizontal: 'center' };
            //console.log('data.produclist[i].lengthhhhh: ', data.produclist.length);
            var totPrice = 0.0;

            for (let i = 0; i < data.produclist.length; i++) {
                if (i == 0) {
                    worksheet.getRow(10).getCell(1).value = parseFloat(data.produclist[i].qty);
                    worksheet.getRow(10).getCell(1).alignment = { vertical: 'middle', horizontal: 'center' };
                    worksheet.getRow(10).getCell(2).value = data.produclist[i].name;
                    worksheet.getRow(10).getCell(2).alignment = { vertical: 'middle', horizontal: 'center' };
                    worksheet.getRow(10).getCell(4).value = parseFloat(data.produclist[i].price);
                    console.log('data.produclist[i].weight: ',data.produclist[i].weight);
                    if(data.produclist[i].weight != ''){
                        worksheet.getRow(10).getCell(4).alignment = { vertical: 'middle', horizontal: 'center' };
                        worksheet.getRow(10).getCell(5).value = data.produclist[i].weight;
                        totPrice = parseFloat(data.produclist[i].price) * parseFloat(data.produclist[i].weight) * parseFloat(data.produclist[i].qty);
                    }else{
                        totPrice = parseFloat(data.produclist[i].price) * parseFloat(data.produclist[i].qty);
                        worksheet.getRow(10).getCell(5).value = '';
                    }
                    worksheet.getRow(10).getCell(5).alignment = { vertical: 'middle', horizontal: 'center' };
                    worksheet.getRow(10).getCell(6).value = totPrice;
                    worksheet.getRow(10).getCell(6).alignment = { vertical: 'middle', horizontal: 'center' };
                    console.log('totPrice primo elemento: ',totPrice);

                    if (i == data.produclist.length - 1) {
                        /* worksheet.getRow(10 + i + 1).getCell(5).border = {
                            bottom: { style: 'thin' },
                            top: { style: 'thin' },
                            right: { style: 'thin' }
                        }; */
                        let rowNumber = 10 + i + 1;
                        worksheet.getRow(10 + i + 1).getCell(5).value = 'Totale Documento';
                        worksheet.getRow(10 + i + 1).getCell(5).alignment = { vertical: 'middle', horizontal: 'center' };
                        worksheet.getCell('E' + rowNumber).border = {
                            bottom: { style: 'thin' },
                            left: { style: 'thin' },
                            right: { style: 'thin' }
                        }
                        worksheet.getRow(10 + i + 1).getCell(6).value = totPrice;
                        worksheet.getRow(10 + i + 1).getCell(6).font = { family: 4, size: 12, bold: true };
                        worksheet.getRow(10 + i + 1).getCell(6).alignment = { vertical: 'middle', horizontal: 'center' };
                        let cont;
                        worksheet.getRow(12 + i + 1).getCell(1).value = 'IBERI.CO SNC DI SIMONE MARABESE E ENZO PIROVANO';
                        worksheet.getRow(12 + i + 1).getCell(1).alignment = { vertical: 'middle', horizontal: 'center' };
                        cont = 12 + i + 1;
                        worksheet.mergeCells('A' + cont, 'F' + cont);
                        worksheet.getRow(12 + i + 1).font = { family: 4, size: 12, bold: true };
                        worksheet.getRow(12 + i + 2).getCell(1).value = 'Indirizzo Sede legale: BRIOSCO (MB) VIA GARIBALDI 1 CAP 20836 FRAZIONE: CAPRIANO Indirizzo PEC: iberi.co@pec.it  - P.IVA: 10588320969';
                        worksheet.getRow(12 + i + 2).getCell(1).alignment = { vertical: 'middle', horizontal: 'center' };
                        cont = 12 + i + 2;
                        worksheet.mergeCells('A' + cont, 'F' + cont);
                        worksheet.getRow(12 + i + 3).getCell(1).value = 'Tel: +39 3335087530 e-mail: info@iberi.co';
                        worksheet.getRow(12 + i + 3).getCell(1).alignment = { vertical: 'middle', horizontal: 'center' };
                        cont = 12 + i + 3;
                        worksheet.mergeCells('A' + cont, 'F' + cont);

                    }

                }
                else {
                    let poudedPrice = 0.0;
                    if(data.produclist[i].weight != ''){
                        poudedPrice = parseFloat(data.produclist[i].qty) * parseFloat(data.produclist[i].price) * parseFloat(data.produclist[i].weight);
                        worksheet.spliceRows(10 + i, 1, [parseFloat(data.produclist[i].qty), data.produclist[i].name, '', parseFloat(data.produclist[i].price) , parseFloat(data.produclist[i].weight), poudedPrice]);
                    }else{
                        poudedPrice = parseFloat(data.produclist[i].qty) * parseFloat(data.produclist[i].price);
                        worksheet.spliceRows(10 + i, 1, [parseFloat(data.produclist[i].qty), data.produclist[i].name, '', parseFloat(data.produclist[i].price), 0 , poudedPrice]);
                    }
                    totPrice = totPrice + poudedPrice;
                    console.log('totPrice: ',totPrice);
                    let cont = 10 + i;
                    worksheet.mergeCells('B' + cont, 'C' + cont);
                    worksheet.getRow(10 + i).getCell(1).border = {
                        bottom: { style: 'thin' },
                        top: { style: 'thin' },
                        right: { style: 'thin' }
                    };
                    worksheet.getRow(10 + i).getCell(1).alignment = { vertical: 'middle', horizontal: 'center' };

                    worksheet.getRow(10 + i).getCell(2).border = {
                        bottom: { style: 'thin' },
                        top: { style: 'thin' },
                        right: { style: 'thin' }
                    };
                    worksheet.getRow(10 + i).getCell(2).alignment = { vertical: 'middle', horizontal: 'center' };

                    worksheet.getRow(10 + i).getCell(4).border = {
                        bottom: { style: 'thin' },
                        top: { style: 'thin' },
                        right: { style: 'thin' }
                    };
                    worksheet.getRow(10 + i).getCell(4).alignment = { vertical: 'middle', horizontal: 'center' };

                    worksheet.getRow(10 + i).getCell(5).border = {
                        bottom: { style: 'thin' },
                        top: { style: 'thin' },
                        right: { style: 'thin' }
                    };
                    worksheet.getRow(10 + i).getCell(5).alignment = { vertical: 'middle', horizontal: 'center' };

                    worksheet.getRow(10 + i).getCell(6).border = {
                        bottom: { style: 'thin' },
                        top: { style: 'thin' },
                        right: { style: 'thin' }
                    };
                    worksheet.getRow(10 + i).getCell(6).alignment = { vertical: 'middle', horizontal: 'center' };

                    if (i == data.produclist.length - 1) {
                        /* worksheet.getRow(10 + i + 1).getCell(5).border = {
                            bottom: { style: 'thin' },
                            top: { style: 'thin' },
                            right: { style: 'thin' }
                        }; */
                        let rowNumber = 10 + i + 1;
                        worksheet.getRow(10 + i + 1).getCell(5).value = 'Totale Documento';
                        worksheet.getRow(10 + i + 1).getCell(5).alignment = { vertical: 'middle', horizontal: 'center' };
                        worksheet.getCell('E' + rowNumber).border = {
                            bottom: { style: 'thin' },
                            left: { style: 'thin' },
                            right: { style: 'thin' }
                        }
                        worksheet.getRow(10 + i + 1).getCell(6).value = totPrice + '€';
                        worksheet.getRow(10 + i + 1).getCell(6).alignment = { vertical: 'middle', horizontal: 'center' };
                        worksheet.getRow(10 + i + 1).getCell(6).font = { family: 4, size: 12, bold: true };
                        worksheet.getRow(12 + i + 1).getCell(1).value = 'IBERI.CO SNC DI SIMONE MARABESE E ENZO PIROVANO';
                        worksheet.getRow(12 + i + 1).getCell(1).alignment = { vertical: 'middle', horizontal: 'center' };
                        worksheet.getRow(12 + i + 1).font = { family: 4, size: 12, bold: true };
                        cont = 12 + i + 1;
                        worksheet.mergeCells('A' + cont, 'F' + cont);
                        worksheet.getRow(12 + i + 2).getCell(1).value = 'Indirizzo Sede legale: BRIOSCO (MB) VIA GARIBALDI 1 CAP 20836 FRAZIONE: CAPRIANO Indirizzo PEC: iberi.co@pec.it  - P.IVA: 10588320969';
                        worksheet.getRow(12 + i + 2).getCell(1).alignment = { vertical: 'middle', horizontal: 'center' };
                        cont = 12 + i + 2;
                        worksheet.mergeCells('A' + cont, 'F' + cont);
                        worksheet.getRow(12 + i + 3).getCell(1).value = 'Tel: +39 3335087530 e-mail: info@iberi.co';
                        worksheet.getRow(12 + i + 3).getCell(1).alignment = { vertical: 'middle', horizontal: 'center' };
                        cont = 12 + i + 3;
                        worksheet.mergeCells('A' + cont, 'F' + cont);


                        /* worksheet.getCell('H21').border = {
                            bottom: { style: 'thin' },
                            left: { style: 'thin' },
                            right: { style: 'thin' }
                        } */

                        /* worksheet.getRow(10 + i + 1).getCell(6).border = {
                            bottom: { style: 'thin' },
                            top: { style: 'thin' },
                            right: { style: 'thin' }
                        }; */
                    }
                }
            }

            workbook.xlsx.writeFile('fatture/' + data.namecustomer[0].replace(/[^a-zA-Z0-9]/g, '') + 'Fattura' + now.getFullYear() + '-' + now.getMonth() + '-' + now.getDate() + '-' + now.getHours() + now.getMinutes() + now.getSeconds() + '.xlsx');

        });
    }


}
