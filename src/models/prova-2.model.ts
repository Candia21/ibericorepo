import {Entity, model, property} from '@loopback/repository';

@model()
export class Prova2 extends Entity {
  @property({
    type: 'array',
    itemType: 'object',
  })
  provaarr?: object[];


  constructor(data?: Partial<Prova2>) {
    super(data);
  }
}
