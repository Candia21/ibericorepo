import { Entity, model, property } from '@loopback/repository';

@model()
export class InvoiceNumber extends Entity {
  @property({
    type: 'number',
    id: true,
  })
  id?: number;

  @property({
    type: 'number',
    id: false,
  })
  number?: number;


  constructor(data?: Partial<InvoiceNumber>) {
    super(data);
  }
}
