import {Entity, model, property} from '@loopback/repository';

@model()
export class Prova extends Entity {
  @property({
    type: 'array',
    itemType: 'string',
  })
  prova?: string[];


  constructor(data?: Partial<Prova>) {
    super(data);
  }
}
