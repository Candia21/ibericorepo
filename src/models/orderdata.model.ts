import { Entity, model, property } from '@loopback/repository';

@model()
export class Orderdata extends Entity {
  @property({
    type: 'number',
    id: true,
  })
  id?: number;

  @property({
    type: 'number',
    id: false,
  })
  invoiceNumber?: number;

  @property({
    type: 'array',
    itemType: 'object',
  })
  produclist: object[];

  @property({
    type: 'array',
    itemType: 'string',
  })
  namecustomer: string[];

  @property({
    type: 'array',
    itemType: 'object',
  })
  customerdetail: object[];


  constructor(data?: Partial<Orderdata>) {
    super(data);
  }
}
