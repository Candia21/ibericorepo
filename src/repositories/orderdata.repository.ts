import {DefaultCrudRepository} from '@loopback/repository';
import {Orderdata} from '../models';
import {OrderdataDataSource} from '../datasources';
import {inject} from '@loopback/core';

export class OrderdataRepository extends DefaultCrudRepository<
  Orderdata,
  typeof Orderdata.prototype.id
> {
  constructor(
    @inject('datasources.orderdata') dataSource: OrderdataDataSource,
  ) {
    super(Orderdata, dataSource);
  }
}
