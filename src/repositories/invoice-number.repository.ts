import {DefaultCrudRepository} from '@loopback/repository';
import {InvoiceNumber} from '../models';
import {InvoiceDataSource} from '../datasources';
import {inject} from '@loopback/core';

export class InvoiceNumberRepository extends DefaultCrudRepository<
  InvoiceNumber,
  typeof InvoiceNumber.prototype.id
> {
  constructor(
    @inject('datasources.invoice') dataSource: InvoiceDataSource,
  ) {
    super(InvoiceNumber, dataSource);
  }
}
