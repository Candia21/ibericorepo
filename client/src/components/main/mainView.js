import React from 'react';
import { ViewModel } from '../../vendor/react/core';
import { Repeater } from '../../vendor/react/components';

export class MainView extends ViewModel {
    render() {
        var altezza = window.screen.height;
        var style = { height: altezza - 350 + 'px', overflowY: 'scroll' }
        var styleCustomer = { overflow: "auto", height: altezza - 350 + 'px' }

        return (
            <div className="App">
                <div className="row">
                    <div className="col-lg-12 " >
                        <div className="row px-5">
                            <div className="col-lg-12" style={{ borderRadius: "10px 10px 10px 10px", backgroundColor: "#8080809e" }}>Generatore Ordini</div>
                        </div>
                        <div className="row px-5">
                            <div className="col-lg-4" style={{ borderRight: "solid 1px" }}>
                                <div className="row">
                                    <div className="col-lg-12" style={{ marginBottom: "5px", marginTop: "5px", fontWeight: "bold" }}>Listino</div>
                                </div>
                                <div className="row">
                                    <div className="col-lg-12 " style={style}>
                                        <div className='row'>
                                            <div className='col-lg-1' style={{ textDecoration: "underline" }}>
                                                Indice
                                            </div>
                                            <div className='col-lg-5' style={{ textDecoration: "underline" }}>
                                                Nome Prodotto
                                            </div>
                                            <div className='col-lg-2' style={{ textDecoration: "underline" }}>
                                                Prezzo €
                                            </div>
                                            <div className='col-lg-2' style={{ textDecoration: "underline" }}>
                                                Quantità
                                            </div>
                                            <div className='col-lg-2' style={{ textDecoration: "underline" }}>
                                                Peso
                                            </div>
                                        </div>
                                        <Repeater data={this.state.products} template={(data, index) => this.templateproduct(data, index)}>
                                            <div />
                                        </Repeater>
                                    </div>
                                </div>
                            </div>
                            <div className="col-lg-4" style={{ borderRight: "solid 1px" }}>
                                <div className="row">
                                    <div className="col-lg-12" style={{ marginBottom: "5px", marginTop: "5px", fontWeight: "bold" }}>Clienti</div>
                                </div>
                                <div className="row">
                                    <div className="col-lg-12" style={styleCustomer}>
                                        <Repeater data={this.state.customers} template={(data, index) => this.templatecustomer(data, index)}>
                                            <div />
                                        </Repeater>
                                        {this.templateAlertCustomer()}
                                    </div>
                                </div>
                            </div>
                            <div className="col-lg-4" >
                                <div className="row">
                                    <div className="col-lg-12" style={{ marginBottom: "5px", marginTop: "5px", fontWeight: "bold" }}>Admin</div>
                                </div>
                                <div className="row">
                                    <div className="col-lg-12" style={{ borderBottom: "solid 1px" }}>

                                        <div className="row">
                                            <div className="col-lg-6" style={{ fontWeight: "bold" }}>Numero Prossima Fattura
                                            </div>
                                            <div className="col-lg-6" >
                                                <div style={{ fontWeight: "bold" }} >{this.state.lastInvoiceNumberShow}</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="row">
                                    <form className="col-lg-12" onSubmit={(e) => this.ctrl.updateInvoiceNumberGui(e)} style={{ marginTop: "15px" }}>
                                        <div className="form-group mx-sm-3 mb-2">
                                            <div className="row">
                                                <div className="col-lg-12" >
                                                    <label className="sr-only">Aggiorna ultima fattura</label>
                                                </div>
                                                <div className="col-lg-6" >
                                                    <input type="number" className="form-control" id="invoiceNumber" placeholder="Numero Fattura" onChange={(e) => this.ctrl.onChangeInvoiceNumber(e)}></input>
                                                </div>
                                                <div className="col-lg-6" >
                                                    <button type="submit" className="btn btn-primary mb-2">Aggiorna numero fattura</button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <div className="row">
                                    <div className="col-lg-12" style={{ borderBottom: "solid 1px", borderTop: "solid 1px", marginTop: "10px" }} >
                                        <form className="" onSubmit={(e) => this.ctrl.addCustomer(e)} style={{ marginTop: "10px" }}>
                                            <div className="form-group ">
                                                <div className="row">
                                                    <div className="col-lg-6" >
                                                        <label className="sr-only ">Nuovo Cliente</label>
                                                        <input type="text" className="form-control" id="newCustomer" placeholder="Nuovo Cliente" onChange={(e) => this.ctrl.onChangeNewCustomer(e)} required></input>
                                                        <input type="text" className="form-control" id="newAddress" placeholder="Indirizzo" onChange={(e) => this.ctrl.onChangeNewCustomer(e)} required></input>

                                                        <input type="text" className="form-control" id="newPIva" placeholder="Partita Iva" onChange={(e) => this.ctrl.onChangeNewCustomer(e)} required></input>
                                                    </div>
                                                    <div className="col-lg-6" >
                                                        <input type="text" className="form-control" id="newPhone" placeholder="Telefono" onChange={(e) => this.ctrl.onChangeNewCustomer(e)} required></input>

                                                        <input type="text" className="form-control" id="newMail" placeholder="Mail" onChange={(e) => this.ctrl.onChangeNewCustomer(e)} required></input>
                                                        <input type="text" className="form-control" id="newPec" placeholder="Pec" onChange={(e) => this.ctrl.onChangeNewCustomer(e)} ></input>
                                                    </div>
                                                </div>
                                            </div>
                                            <button type="submit" className="btn btn-primary mb-2" >Aggiungi Cliente</button>
                                            {this.templateAlertAddCustomer()}
                                        </form>
                                    </div>
                                    <form className="col-lg-12" onSubmit={() => this.ctrl.deleteCustomer()} style={{ marginTop: "15px", borderBottom: "solid 1px" }}>
                                        <div className="form-group mx-sm-3 mb-2">
                                            <label className="sr-only">Cancella Cliente</label>
                                            <input type="text" className="form-control" id="deleteCustomer" placeholder="ID Cliente" onChange={(e) => this.ctrl.onChangeDeleteCustomer(e)}></input>
                                        </div>
                                        <button type="submit" className="btn btn-primary mb-2">Elimina Cliente</button>
                                        {this.templateAlertDeleteCustomer()}

                                    </form>
                                    <div className="col-lg-12" >
                                        <p style={{ marginBottom: "5px", marginTop: "5px", fontWeight: "bold" }}>Dettagli cliente</p>
                                        {this.templatecustomerDetail()}
                                    </div>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-lg-12" >
                                    {this.state.customer}
                                </div>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-lg-12 pb-3" id="footer" style={{ paddingTop: "25px", borderTop: "solid 1px", bottom: "0" }} >
                                <div className="row">
                                    <div className="col-lg-2" ></div>
                                    <div className="col-lg-8 "  >
                                        <button type="button" className="btn btn-success btn-lg btn-block" style={{ borderRadius: "15px" }} disabled={this.ctrl.disableButtonOrder()} onClick={() => this.ctrl.generateOrder()}>Genera Ordine</button>
                                        {this.templateAlertOrder()}
                                        {this.templateAlertOrderSuccess()}
                                    </div>
                                </div>
                            </div>
                        </div>


                    </div>
                </div>

            </div >
        )
    }

    templatecustomer(data, index) {
        return (
            <div className="modal-list select-script" key={index} >
                <div className='row'>
                    <div className='col-lg-1' >
                        {data.id}
                    </div>
                    <div className='col-lg-5'>
                        {data.name}
                    </div>
                    <div className='col-lg-5'>
                        <button type="button" className="btn btn-outline-dark" onClick={(e) => this.ctrl.selectCustomer(e, data, index)} >Sel.</button>
                    </div>
                </div>
            </div>
        )
    }

    templateproduct(data, index) {
        if (!data.withWeight) {
            return (
                <div key={index} >
                    <div className='row'>
                        <div className='col-lg-1'>
                            {index}
                        </div>
                        <div className='col-lg-5'>
                            {data.name}
                        </div>
                        <div className='col-lg-2' >
                            <input type="number" id="price" placeholder="Pz" onChange={(e) => this.ctrl.onChange(e, index)} style={{ width: "60px" }} value={this.state.products[index].price} ></input>
                    </div>
                        <div className='col-lg-2'>
                            <input type="number" id="qty" placeholder="Qtà" onChange={(e) => this.ctrl.onChange(e, index)} style={{ width: "60px" }}></input>
                        </div>
                        <div className='col-lg-2'>
                        </div>
                    </div>
                </div>
            )
        } else {
            return (
                <div key={index} >
                    <div className='row'>
                        <div className='col-lg-1'>
                            {index}
                        </div>
                        <div className='col-lg-5'>
                            {data.name}
                        </div>
                        <div className='col-lg-2' >
                        <input type="number" id="price" placeholder="Pz" onChange={(e) => this.ctrl.onChange(e, index)} style={{ width: "60px" }} value={this.state.products[index].price} ></input>
                    </div>
                        <div className='col-lg-2'>
                            <input type="number" id="qty" placeholder="Qty" onChange={(e) => this.ctrl.onChange(e, index)} style={{ width: "60px" }}></input>
                        </div>
                        <div className='col-lg-2'>
                            <input type="number" id="weight" placeholder="Peso" onChange={(e) => this.ctrl.onChange(e, index)} style={{ width: "60px" }}></input>
                        </div>
                    </div>
                </div>
            )
        }

    }

    templatecustomerDetail() {
        if (!this.state.showCustomerDetail) {
            return (
                <div className='row' >
                    <div className='col-lg-12' >
                        <p>Seleziona un cliente per vedere i dettagli.</p>
                    </div>
                </div >
            )
        } else {
            return (
                <div className='row' >
                    <div className='col-lg-12' >
                        <form className="col-lg-12" onSubmit={(e) => this.ctrl.modifyCustomer(e)} style={{ marginTop: "15px" }}>
                            <div className='row' >
                                <div className='col-lg-6' >
                                    <p>name:</p>
                                </div>
                                <div className='col-lg-6' >
                                    <input type="text" className="form-control" id="name" placeholder="Nome Cliente" value={this.state.customerDetail.name || ''} onChange={(e) => this.ctrl.onChangeCustomer(e)}></input>
                                </div>
                            </div >
                            <div className='row' >
                                <div className='col-lg-6' >
                                    <p>address:</p>
                                </div>
                                <div className='col-lg-6' >
                                    <input type="text" className="form-control" id="address" placeholder="Indirizzo" value={this.state.customerDetail.address || ''} onChange={(e) => this.ctrl.onChangeCustomer(e)}></input>
                                </div>
                            </div >
                            <div className='row' >
                                <div className='col-lg-6' >
                                    <p>piva:</p>
                                </div>
                                <div className='col-lg-6' >
                                    <input type="text" className="form-control" id="piva" placeholder="Partita Iva" value={this.state.customerDetail.piva || ''} onChange={(e) => this.ctrl.onChangeCustomer(e)}></input>
                                </div>
                            </div >
                            <div className='row' >
                                <div className='col-lg-6' >
                                    <p>phone:</p>
                                </div>
                                <div className='col-lg-6' >
                                    <input type="number" className="form-control" id="phone" placeholder="Telefono" value={this.state.customerDetail.phone || ''} onChange={(e) => this.ctrl.onChangeCustomer(e)}></input>
                                </div>
                            </div >
                            <div className='row' >
                                <div className='col-lg-6' >
                                    <p>email:</p>
                                </div>
                                <div className='col-lg-6' >
                                    <input type="text" className="form-control" id="email" placeholder="Email" value={this.state.customerDetail.email || ''} onChange={(e) => this.ctrl.onChangeCustomer(e)}></input>
                                </div>
                            </div >
                            <div className='row' >
                                <div className='col-lg-6' >
                                    <p>pec:</p>
                                </div>
                                <div className='col-lg-6' >
                                    <input type="text" className="form-control" id="pec" placeholder="Pec" value={this.state.customerDetail.pec || ''} onChange={(e) => this.ctrl.onChangeCustomer(e)}></input>
                                </div>
                            </div >
                            <button type="submit" className="btn btn-primary mb-2">Modifica Dati cliente</button>
                        </form>
                        {this.templateAlertModifyCustomer()}
                    </div>
                </div >
            )
        }
    }

    templateAlertCustomer() {
        if (this.state.showAlertCustomer) {
            return (
                <div className="alert alert-danger alert-dismissible">
                    <strong>Clienti non caricati!</strong> Attendere che il server sia attivo.

                </div>
            )
        } else {
            return (
                <div >
                </div>
            )
        }
    }

    templateAlertModifyCustomer() {
        if (this.state.showAlertModifyCustomer == 'Error') {
            return (
                <div className="alert alert-danger alert-dismissible">
                    <strong>Qualcosa è andato storto</strong> riprova più tardi.

                </div>
            )
        } else if (this.state.showAlertModifyCustomer == 'Success') {
            return (
                <div >
                    <div className="alert alert-success alert-dismissible">
                        <strong>Cliente Aggiornato!</strong>

                    </div>
                </div>
            )
        } else {

            return (
                <div >
                </div>
            )
        }
    }

    templateAlertAddCustomer() {
        if (this.state.showAlertCreateFolder === 2) {
            return (
                <div className="alert alert-danger alert-dismissible">
                    <strong>Cartella non creata!</strong>

                </div>
            )
        } else if (this.state.showAlertCreateFolder === 1) {
            return (
                <div >
                    <div className="alert alert-success alert-dismissible">
                        <strong>Cartella creata!</strong>

                    </div>
                </div>
            )
        }
    }

    templateAlertDeleteCustomer() {
        if (this.state.showAlertDeleteCustomer) {
            return (
                <div className="alert alert-danger alert-dismissible">
                    <strong>Cliente non cancellato!</strong>

                </div>
            )
        } else {
            return (
                <div >
                </div>
            )
        }
    }

    templateAlertOrder() {
        if (this.state.showAlertCreateOrder) {
            return (
                <div className="alert alert-danger alert-dismissible">
                    <strong>Ordine non generato</strong>

                </div>
            )
        } else {
            return (
                <div >
                </div>
            )
        }
    }

    templateAlertOrderSuccess() {
        if (this.state.showAlertCreateOrderSuccess) {
            return (
                <div className="alert alert-success alert-dismissible">
                    <strong>Ordine generato</strong>

                </div>
            )
        } else {
            return (
                <div >
                </div>
            )
        }
    }

}
