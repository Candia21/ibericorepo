import React from 'react';
import { Controller } from '../../vendor/react/core';
import { MainView } from './';
import { RestManager, PopulateState } from '../../utils';


export class Main extends Controller {

    constructor(props) {
        super(props, MainView);

        var refreshCustomers = () => {
            return RestManager.applyStrategy({
                method: 'GET',
                url: '/customers'
            }).then((result) => {
                this.mergeState({
                    customers: result,
                    showAlertCustomer: false
                })
            }).catch((error) => {
                console.log('error: ', error);
                this.mergeState({
                    showAlertCustomer: true
                })
                window.setTimeout(() => { refreshCustomers() }, 2000);
            })
        }
        var getInvoiceNumber = () => {
            return RestManager.applyStrategy({
                method: 'GET',
                url: '/invoice-numbers'
            }).then((result) => {
                console.log('result invoice: ', result[0]);
                this.mergeState({
                    lastInvoiceNumberShow: result[0].number
                })
            }).catch((error) => {
                console.log('error: ', error);
                window.setTimeout(() => { getInvoiceNumber() }, 2000);
            })
        }

        this.state = {
            products: PopulateState.populateProductState(),
            lastInvoiceNumber: '',
            lastInvoiceNumberShow: '',
            customers: [],
            newCustomer: '',
            newAddress: '',
            newPIva: '',
            newPhone: '',
            newMail: '',
            newPec: '',
            customerDetail: '',
            deleteCustomer: '',
            selectedCustomer: '',
            showCustomerDetail: false,
            showAlertCustomer: false,
            showAlertCreateFolder: '',
            showAlertDeleteCustomer: false,
            showAlertCreateOrder: false,
            showAlertCreateOrderSuccess: false,
            showAlertModifyCustomer: ''
        }

        refreshCustomers().then((result) => {
            getInvoiceNumber();
        });


    }



    onChange(e, index) {
        e.preventDefault();
        console.log('target', e.target);
        let prod = this.state.products;
        if (e.target.id === 'qty') prod[index].qty = e.target.value;
        else if (e.target.id === 'weight') prod[index].weight = e.target.value;
        else if (e.target.id === 'price') prod[index].price = e.target.value;

        this.mergeState({
            products: prod
        })
    }
    onChangeInvoiceNumber(e, index) {
        this.mergeState({
            lastInvoiceNumber: e.target.value
        })
    }

    onChangeNewCustomer(e) {
        console.log('nuovoCliente: ', e.target.id);
        this.mergeState({
            [e.target.id]: e.target.value
        })
    }
    onChangeCustomer(e) {
        var customerDetailTemp = this.state.customerDetail;
        customerDetailTemp[e.target.id] = e.target.value;
        this.mergeState({
            customerDetail: customerDetailTemp
        })
        console.log('vecchioCliente: ', customerDetailTemp);

    }

    modifyCustomer(e) {
        e.preventDefault();
        let data = {
            "id": parseInt(this.state.customerDetail['id']),
            "name": this.state.customerDetail['name'],
            "address": this.state.customerDetail['address'],
            "piva": this.state.customerDetail['piva'],
            "phone": this.state.customerDetail['phone'],
            "email": this.state.customerDetail['email'],
            "pec": this.state.customerDetail['pec']
        }
        RestManager.applyStrategy({
            method: 'PUT',
            url: '/customers/' + this.state.customerDetail['id'],
            data: data
        }).then((result) => {
            this.mergeState({
                showAlertModifyCustomer: 'Success'
            })
            window.setTimeout(() => {
                this.mergeState({
                    showAlertModifyCustomer: ''
                })
            }, 5000);
            return RestManager.applyStrategy({
                method: 'GET',
                url: '/customers'
            }).then((result) => {
                this.mergeState({
                    customers: result,
                    showAlertCustomer: false
                })
            }).catch((error) => {
                console.log('error: ', error);
                this.mergeState({
                    showAlertCustomer: true
                })
            })
        }
        ).catch((error) => {
            this.mergeState({
                showAlertModifyCustomer: 'Error'
            })
            window.setTimeout(() => {
                this.mergeState({
                    showAlertModifyCustomer: ''
                })
            }, 5000);
        })
    }

    onChangeDeleteCustomer(e) {
        this.mergeState({
            deleteCustomer: e.target.value
        })
    }

    addCustomer(e) {
        e.preventDefault();
        let data = {};
        if(typeof this.state.customers !== 'undefined' && this.state.customers.length > 0){
            data = {
                "id": parseInt(this.state.customers.slice(-1).pop().id) + 1,
                "name": this.state.newCustomer,
                "address": this.state.newAddress,
                "piva": this.state.newPIva,
                "phone": parseInt(this.state.newPhone),
                "email": this.state.newMail,
                "pec": this.state.newPec
            }
        }else{
            data = {
                "id": 1,
                "name": this.state.newCustomer,
                "address": this.state.newAddress,
                "piva": this.state.newPIva,
                "phone": parseInt(this.state.newPhone),
                "email": this.state.newMail,
                "pec": this.state.newPec
            }
        }
        console.log('addCustomer: ', data)
        RestManager.applyStrategy({
            method: 'POST',
            url: '/customers',
            data: data
        }).then((result) => {
            console.log(result)
            let cust = this.state.customers;
            cust.push(data);
            this.mergeState({
                newCustomer: '',
                customers: cust,
                showAlertCreateFolder: 1
            })
        }
        ).catch((error) => {

            console.log('error add customer: ', error.details);
            this.mergeState({
                showAlertCreateFolder: 2
            })
        })
    }

    deleteCustomer() {
        RestManager.applyStrategy({
            method: 'DELETE',
            url: '/customers/' + parseInt(this.state.deleteCustomer)
        }).then((result) => {
            let cust = this.state.customers;
            cust.splice(this.state.deleteCustomer - 1, 1);
            console.log(cust)
            this.mergeState({
                deleteCustomer: '',
                customers: cust
            })
        }
        ).catch((error) => {
            console.log('error: ', error);
            this.mergeState({
                showAlertDeleteCustomer: true
            })
        })
    }

    selectCustomer(e, data, index) {
        console.log('selCustomer: ', data);

        if (data === this.state.customerDetail) {
            this.setState({
                selectedCustomer: '',
                customerDetail: '',
                showCustomerDetail: false
            })
        } else {
            this.setState({
                selectedCustomer: data.name,
                customerDetail: data,
                showCustomerDetail: true
            })
        }
    }

    getOffset(el) {
        const rect = document.getElementById(el).getBoundingClientRect();
        return {
            left: rect.left + window.scrollX,
            top: rect.top + window.scrollY
        };
    }

    disableButtonOrder() {
        let flag = true;
        for (let i = 0; i < this.state.products.length; i++) {
            if (this.state.products[i].qty !== '' && this.state.products[i].qty > 0) {
                if (this.state.products[i].withWeight === true && (this.state.products[i].weight === '' || this.state.products[i].weight <= 0)) flag = true;
                else flag = false;
            }
        }
        return flag || this.state.selectedCustomer === '';
    }

    cleanStatus() {
        let prod = this.state.products;
        for (let i = 0; i < prod.length; i++) {
            prod[i].qty = '';
            console.log('prodottiqty', prod[i].qty);

        }

        this.mergeState({
            selectedCustomer: '',
            products: prod,
            showCustomerDetail: false
        })
    }

    updateInvoiceNumberGui(e) {
        e.preventDefault();
        console.log('eee: ', e);
        RestManager.applyStrategy({
            method: 'PUT',
            url: '/invoice-numbers/1',
            data: {
                "id": 1,
                "number": parseInt(this.state.lastInvoiceNumber)
            }
        }).then((result) => {
            console.log('PUT result: ', result);
            this.mergeState({
                lastInvoiceNumberShow: this.state.lastInvoiceNumber
            })
        }
        ).catch((error) => {
            console.log('error: ', error);
        })

    }

    updateInvoiceNumber() {
        RestManager.applyStrategy({
            method: 'PUT',
            url: '/invoice-numbers/1',
            data: {
                "id": 1,
                "number": parseInt(this.state.lastInvoiceNumberShow) + 1
            }
        }).then((result) => {
            console.log('PUT result: ', result);
            this.mergeState({
                lastInvoiceNumberShow: this.state.lastInvoiceNumber
            })
        }
        ).catch((error) => {
            console.log('error: ', error);
        })

    }

    getInvoiceNumber() {
        return RestManager.applyStrategy({
            method: 'GET',
            url: '/invoice-numbers'
        }).then((result) => {
            console.log('result invoice: ', result[0]);
            this.mergeState({
                lastInvoiceNumberShow: result[0].number
            })
        }).catch((error) => {
            console.log('error: ', error);
        })
    }

    generateOrder() {
        let data = {
            "id": 0,
            "invoiceNumber": '',
            "produclist": [],
            "namecustomer": [],
            "customerdetail": []
        }
        data.invoiceNumber = parseInt(this.state.lastInvoiceNumberShow);
        for (let i = 0; i < this.state.products.length; i++) {

            if (this.state.products[i].qty != null && this.state.products[i].qty && this.state.products[i].qty !== '') {
                let el = { "name": this.state.products[i].name, "price": this.state.products[i].price, "qty": this.state.products[i].qty, "weight": this.state.products[i].weight, "withWeight": this.state.products[i].withWeight };
                data.produclist.push(el);
            }
        }
        if (this.state.selectedCustomer != null && this.state.selectedCustomer !== '') {
            data.namecustomer.push(this.state.selectedCustomer);
            console.log('customerDetail: ', this.state.customerDetail);
            data.customerdetail.push(this.state.customerDetail);

        }


        RestManager.applyStrategy({
            method: 'POST',
            url: '/orderdata',
            data: data
        }).then((result) => {
            console.log('resultPOST: ', result)
            this.mergeState({
                showAlertCreateOrderSuccess: true
            })
            this.updateInvoiceNumber();
            this.getInvoiceNumber();
            window.setTimeout(() => { this.cleanStatus(); window.location.reload(); }, 3000);
        }
        ).catch((error) => {
            console.log('error: ', error);
            this.mergeState({
                showAlertCreateOrder: true
            })
        })
    }


}
