import { Rest } from '../vendor/utils';

export class RestManager {

    static get BASE_URL() { return 'http://localhost:3000' }


    static loadData() {
        return RestManager.applyStrategy({
            method: 'GET',
            url: '/servicies/load-data'
        })
    }

    

	/**	
	 * Strategy
	 */
    static applyStrategy(data) {
        data.url = RestManager.BASE_URL + data.url;

        data.contentType = 'application/json';
        data.data = JSON.stringify(data.data);
        
        return new Promise((resolve, reject) => {
            Rest.send(data).then((e) => {
                resolve(e);
            }).catch((e) => {
                reject(e)
            });
        })
    }


}