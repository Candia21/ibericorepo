
export class PopulateState {

  static populateProductState() {
    var product = [
      {
        name: 'Bellota coscia intera 36 mesi',
        qty: '',
        price: 62,
        weight: '',
        withWeight: true
      },
      {
        name: 'Cebo de campo coscia intera 30 mesi',
        qty: '',
        price: 33,
        weight: '',
        withWeight: true
      },
      {
        name: 'Bellota coscia intera 100% patanegra',
        qty: '',
        price: 71,
        weight: '',
        withWeight: true
      },
      {
        name: 'Centro de paleta reserva 24 mesi',
        qty: '',
        price: 33,
        weight: '',
        withWeight: true
      },
      {
        name: 'Lomo gran reserva',
        qty: '',
        price: 33,
        weight: '',
        withWeight: true
      },
      {
        name: 'Salchichoncitos',
        qty: '',
        price: 33,
        weight: '',
        withWeight: true
      },
      {
        name: 'Chorizo piccante',
        qty: '',
        price: 14,
        weight: '',
        withWeight: true
      },
	{
        name: 'Salchichon bellota',
        qty: '',
        price: 15,
        weight: '',
        withWeight: true
      },
	{
        name: 'Jamon 36 mesi busta sottovuoto',
        qty: '',
        price: 19,
        weight: '',
        withWeight: true
      },
	{
        name: 'Jamon 30 mesi busta sottovuoto',
        qty: '',
        price: 12,
        weight: '',
        withWeight: true
      },
	{
        name: 'Paleta 24 mesi 50% busta sottovuoto',
        qty: '',
        price: 8,
        weight: '',
        withWeight: true
      },
	{
        name: 'Lomo 50% busta sottovuoto',
        qty: '',
        price: 8,
        weight: '',
        withWeight: true
      },
	{
        name: 'Torta dehesa',
        qty: '',
        price: 8,
        weight: '',
        withWeight: false
      },
    	{
        name: 'Queso oveja al vino syrah',
        qty: '',
        price: 23,
        weight: '',
        withWeight: false
      },
	{
        name: 'Queso oveja curado',
        qty: '',
        price: 20,
        weight: '',
        withWeight: false
      },
	{
        name: 'Queso oveja curado',
        qty: '',
        price: 20,
        weight: '',
        withWeight: false
      },
	{
        name: 'Queso oveja curado cuña',
        qty: '',
        price: 5,
        weight: '',
        withWeight: false
      },
	{
        name: 'Queso cabra tomillo',
        qty: '',
        price: 26,
        weight: '',
        withWeight: false
      },
	{
        name: 'Hacienda Zorita Syrah',
        qty: '',
        price: 9.5,
        weight: '',
        withWeight: false
      },
	{
        name: 'Hacienda Zorita Magister',
        qty: '',
        price: 18,
        weight: '',
        withWeight: false
      },
	{
        name: 'Cava Nature',
        qty: '',
        price: 7.5,
        weight: '',
        withWeight: false
      },
	{
        name: 'Marques de Griñon Verdejo',
        qty: '',
        price: 8,
        weight: '',
        withWeight: false
      },
	{
        name: 'Marques de Griñon Rioja',
        qty: '',
        price: 12.5,
        weight: '',
        withWeight: false
      },
	{
        name: 'Marques de Griñon Summa Varietalis',
        qty: '',
        price: 22,
        weight: '',
        withWeight: false
      },
	{
        name: 'Marques de Griñon Aceite DUO',
        qty: '',
        price: 8.5,
        weight: '',
        withWeight: false
      },
	{
        name: 'Hacienda Zorita Aceite',
        qty: '',
        price: 14,
        weight: '',
        withWeight: false
      }
];

    return product;
  }
}
