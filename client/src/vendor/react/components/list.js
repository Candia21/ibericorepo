import React, { Component } from 'react';

import { Repeater } from ".";

export class List extends React.Component {

	constructor(props){
		super(props);
		
		this.state = {
			values: this.wrapper(props.values),
			tmpAddValue: ""
		}
	}

	add(value){
		var values = this.state.values;

		values.push({value: value});

		this.setState({values: values});
		this.change(values);
	}

	remove(index){
		var values = this.state.values;

		values.splice(index, 1);

		this.setState({values: values});
		this.change(values);
	}

	change(values){
		var callback = this.props.onChange || function(){};

		var valuesClean = [];

		for(var index in values){
			var value = values[index];

			valuesClean.push(value.value);
		}


		callback(valuesClean);
	}

	wrapper(e){
		var valuesClean = [];
		for(var index in e){
			var value = e[index];

			valuesClean.push({value: value});
		}


		return valuesClean;
	}

	onChange(e){
		var value = e.target.value;

		this.setState({tmpAddValue: value});
	}

	onClick(e){
		this.add(this.state.tmpAddValue)

		this.setState({tmpAddValue: ""});
	}

	onClickRemove(e, index){
		this.remove(index);
	}

	template(data, index) {
		return (
			<li key={index} role="presentation" className="badge">
				{data.value} <span className="glyphicon glyphicon-remove" onClick={(e)=>this.onClickRemove(e, index)}></span>
			</li>
		);
	}

	render(){
		return (
			<div>
				<div className="input-group">
					<input type="text" className="form-control" onChange={(e)=>this.onChange(e)} value={this.state.tmpAddValue}/>
					<span className="input-group-btn">
						<button className="btn btn-default" onClick={(e)=>this.onClick(e)} type="button">Add</button>
					</span>
				</div>
				<br/>
				<Repeater data={this.state.values} template={(data,index)=>this.template(data,index)}>
					<ul className="nav nav-pills list-container" role="tablist" />
				</Repeater>
			</div>
		)
	}

	componentWillReceiveProps (nextProps){
		this.setState({values: this.wrapper(nextProps.values)})
	}
}	