import React, { Component } from 'react';
import { render } from 'react-dom'
import { Service } from '../../utils';

export class Singleton extends Component {

	constructor(props){
		super(props);

		this.ref = null;
		this.component = <this.props.component {...this.props} ref={ref => { Service.set(this.props.name, ref)}}/>

		this.state = {
			component: this.component,
		}
	}

	render(){
		return this.state.component
	}
}