import React, { Component } from 'react';

export class ChangeValue extends Component {

	constructor(props){
		super(props);

		this.state = {
			rate: 0,
			delta: 0
		}

		this.interval = null;
	}

	render(){
		var style = {
			position: "relative",
			display: "inline-block",
		};

		if(this.state.rate < 0){
			style.color = "#dc3545";
		}else if (this.state.rate > 0){
			style.color = "#28a745";
		}

		var value = Number(this.props.value);

		return(
			<span style={style}>
				{this.props.prepend}

				{value.toFixed(this.props.fixed || 2)}

				{this.props.append}

				{this.renderDelta()}
			</span>
		)
	}

	renderDelta(){
		if (this.state.delta == 0) return null;
		
		var styleChild = {
		    position: "absolute",
		    zIndex: "1",
		    marginTop: (this.props.offsetY) ? this.props.offsetY : '-15px',
		    marginLeft: (this.props.offsetY) ? this.props.offsetX : '0px',
		}

		return (
			<small style={styleChild}>
				{(this.state.delta > 0 ) ? '+' : ''}

				{this.state.delta.toFixed(this.props.toFixed || 2)}
			</small>
		)
	}

	setDefault(){
		this.setState({
			rate: 0,
			delta: 0
		})
	}

	componentWillReceiveProps(nextProps){
		var value = this.props.value;

		var newValue = nextProps.value;

		var rate = 0;

		var delta = newValue - value;

		if(delta < 0){
			rate = -1;
		}else if (delta > 0){
			rate = 1;
		}

		this.setState({
			rate: rate,
			delta: delta
		})

		this.interval = setTimeout(()=>this.setDefault(), 1000);
	}

	componentWillUnmount(){
		clearInterval(this.interval)
	}
}