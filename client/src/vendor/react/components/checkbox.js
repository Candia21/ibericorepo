import React, { Component } from 'react';

export class Checkbox extends Component {

	constructor(props){
		super(props);

		this.state = {
			checked: (this.props.value) ? this.props.value : false
		}
	}

	onClick(e){
		this.setState({
			checked: !this.state.checked
		})

		if(typeof this.props.onChange != "undefined"){
			this.props.onChange(
				{
					target: {
						name: this.props.name,
						value: !this.state.checked
					}
				}
			);
		}
	}

	getStyle(){
		return {
			backgroundPosition: (this.state.checked) ? "-268px -309px" : "-308px -309px",
			width: "24px",
			height: "24px",
			backgroundRepeat: "no-repeat",
			cursor: "pointer",
			display: "inline-block",
    		verticalAlign: "middle"
		}
	}

	render() {
		return (
			<span className="fw-checkbox" onClick={(e)=>this.onClick(e)} style={this.getStyle()}></span>
		);
	}
}
