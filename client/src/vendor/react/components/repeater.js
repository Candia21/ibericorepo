import React, { Component } from 'react';

export class Repeater extends React.Component {

	constructor(props){
		super(props);
	}

	core() {
		var content = [];

		//content.push(this.props.children.props.children);
		if(this.props.prepend != null){
			for(var key in  this.props.prepend){
				var value = this.props.prepend[key];
				if(typeof value == "object") value._index = `prepend-${key}`;

				var template = this.props.template(value, `prepend-${key}`)

				content.push(template);
			}
		}

		for(var i in this.props.data){
			if (isNaN(i) && this.props.stringKey == false) continue;

			if(this.props.stringKey && this.contain(i))continue;

			var value = this.props.data[i];
			
			if(typeof value == "object") value._index = i;

			var template = this.props.template(value, i)

			content.push(template);
		}

		if(this.props.append != null){
			for(var key in  this.props.append){
				var value = this.props.append[key];

				if(typeof value == "object") value._index = `append-${key}`;

				var template = this.props.template(value, `append-${key}`)

				content.push(template);
			}
		}

		return React.createElement(this.props.children.type, this.props.children.props, content);
	}

	contain(keyFind){
		for(var key in this.props.excludeKey){
			var value = this.props.excludeKey[key];

			if (value == keyFind){
				return true;
			}
		}

		return false;
	}

	render(){
		return this.core();	
	}
}