import React, { Component } from 'react';

export class Render extends Component {

	constructor(props){
		super(props);
		this.state = {};
	}

	render(){
		if(!this.props.condition) return null;
		return this.props.children;
	}

}
