import { connect } from 'react-redux'

export class ReduxContainer {
	mapStateToProps(state){
		return state;
	}

	mapDispatchToProps(dispatch){
		return {}
	}

	connect(component){
		return connect(this.mapStateToProps,this.mapDispatchToProps)(component);
	}
}