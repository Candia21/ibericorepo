export class ViewModel {

	constructor(that){
		this._ctrl = that;

		for(var key in that){
			this[key] = that[key];
		}

	}

	set ctrl (value){ };
	get ctrl () { return this._ctrl}

	onChange(e){
		var value = e.target.value;
		var name = e.target.name;

		this.ctrl.setState({
			[name]: value
		})
	}

}
