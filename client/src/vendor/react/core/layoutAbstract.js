import React, { Component } from 'react';

import { Controller } from '.';

export class LayoutAbstract extends Controller{

	constructor(props){
		super(props);
	}

	renderContent(){
		return React.createElement(this.props.module, this.props);
	}


}