import React, { Component } from 'react';

import { Global } from "../../utils";

export class Controller extends Component {

	constructor(props, view){
		super(props);
		this._view = view;
	}

	set view (value) {}
	get view () { 
		return new this._view(this)
	}


	mergeState(state, callback){
		this.setState(Global.merge(this.state, state), callback);
	}

	render(){
		return (this.view) ? this.view.render() : null;
	}

	onChange(e){
		var name = e.target.name;
		var value = e.target.value;

		this.setState({
			[name]:value
		})

	}

}
