import { Global } from "./../utils";

export class Validation {

	constructor(rules){
		this.rules = rules;


		this.state = {

		}

		this.messages = this.rules.messages || {};

		for(var key in this.rules){
			for(var ruleKey in this.rules[key]){
				this.state = Global.merge(this.state, {
					[ruleKey]: {
						validation: true,
						minLength: false,
						maxLength: false,
						required: true,
						minValue: false,
						maxValue: false,
						value: ''
					}
				});
			}
		}
	}

	validate(object){
		for(var key in object){
			this.execute(key, object[key]);
		}	
	}

	execute(name, value){
		var object = {
			validation: true,
			minLength: false,
			maxLength: false,
			required: true,
			minValue: false,
			maxValue: false,
			messages: [],
			value: (typeof this.state[name] != 'undefined') ? this.state[name].value : ''
		}

		if(typeof this.rules.maxLength != 'undefined' && typeof this.rules.maxLength[name] != 'undefined'){
			if(value.length > this.rules.maxLength[name]){
				object.maxLength = true;
				if(typeof this.messages[name] != 'undefined' && this.messages[name].maxLength) object.messages.push(this.messages[name].maxLength);
			}
		}

		if(typeof this.rules.minLength != 'undefined' && typeof this.rules.minLength[name] != 'undefined'){
			if(value.length < this.rules.minLength[name]){
				object.minLength = true;
				if(typeof this.messages[name] != 'undefined' && this.messages[name].minLength) object.messages.push(this.messages[name].minLength);
			}
		}


		if(!isNaN(object.value)){
			if(typeof this.rules.minValue != 'undefined' && typeof this.rules.minValue[name] != 'undefined' && this.rules.minValue[name]){
				if(value != '' && value < this.rules.minValue[name]){
					object.minValue = true;
					value = this.rules.minValue[name];
					if(typeof this.messages[name] != 'undefined' && this.messages[name].minValue) object.messages.push(this.messages[name].minValue);
				}	
			}

			if(typeof this.rules.maxValue != 'undefined' && typeof this.rules.maxValue[name] != 'undefined' && this.rules.maxValue[name]){
				if(value != '' && value > this.rules.maxValue[name]){
					object.maxValue = true;			
					value = this.rules.maxValue[name];
					if(typeof this.messages[name] != 'undefined' && this.messages[name].maxValue) object.messages.push(this.messages[name].maxValue);
				}	
			}
		}

		if(!object.maxLength || object.minValue || object.maxValue){
			object.value = value;
		}

		if(typeof this.rules.validation != 'undefined' && typeof this.rules.validation[name] != 'undefined'){
			object.validation = Global.validation(this.rules.validation[name], object.value).isValid();
			if(typeof this.messages[name] != 'undefined' && this.messages[name].validation) object.messages.push(this.messages[name].validation);
		}

		if(typeof this.rules.required != 'undefined' && typeof this.rules.required[name] != 'undefined' && this.rules.required[name]){
			if(object.value != ''){
				object.required = false;
			}else{
				if(typeof this.messages[name] != 'undefined' && this.messages[name].required) object.messages.push(this.messages[name].required);
			}	
		}else{
			object.required = false;
		}


		if(typeof this.rules.custom != 'undefined' && typeof this.rules.custom[name] != 'undefined'){
		
			object.validation = (object.validation) ? this.rules.custom[name](object.value) : object.validation;
		}


		this.state = Global.merge(this.state, {[name]: object});

		return object;
	}

	isValid(){
		for(var key in this.state){
			if( !this.isValidRule(key) ){
				return false;
			}
		}

		return true;
	}

	isValidRule(key){
		if( this.state[key].required || this.state[key].minLength || !this.state[key].validation){
			return false;
		}
		return true;
	}
}
