export class Service{
	static initComponentList(name){
		if(typeof window._component == "undefined") window._component = {};
		if(typeof window._component[name] == "undefined") window._component[name] = [];
	}

	static initComponent(name){
		if(typeof window._component == "undefined") window._component = {};
		if(typeof window._component[name] == "undefined") window._component[name] = {};
	}

	static get(name){
		this.initComponent(name);

		return window._component[name]
	}

	static set(name, object){
		this.initComponent(name);

		window._component[name] = object;
	}

	static add(name, object){
		this.initComponentList(name);

		window._component[name].push(object);
	}

	static remove(name, index){
		this.initComponentList(name);

		window._component[name].splice(index, 1);
	}

	static select(name, index){
		this.initComponentList(name);

		return window._component[name][index];	
	}
}