import $ from "jquery";

export class Global{

	static merge(){
		var result = {}, obj;
		for (var i = 0; i < arguments.length; i++) {
			obj = arguments[i];
			for (var key in obj) {
				if (Object.prototype.toString.call(obj[key]) === '[object Object]') {
					if (typeof result[key] === 'undefined') {
						result[key] = {};
					}
					result[key] = this.merge(result[key], obj[key]);
				} 
				else {
					result[key] = this.compare(result[key],obj[key]);
				}
			}
		}
		return result;
	}

	static clone(obj){
		return this.merge({},obj);
	}


	static compare(a,b){
		return b;
	}

	static downloadFile(value, nameFile){
		var url = URL.createObjectURL( new Blob( [value], {type:'text/plain'} ) );
		var anchor = document.createElement('a');
		anchor.href = url;
		anchor.target = '_blank';
		anchor.download = nameFile;
		anchor.click();	
	}

	static validation(regExObject, str){
		var regex = regExObject;
		let m;

		var values = {
			matchs: [],
			isValid: function () {
				return this.matchs.length > 0
			}
		};


		while ((m = regex.exec(str)) !== null) {
			// This is necessary to avoid infinite loops with zero-width matches
			if (m.index === regex.lastIndex) {
				regex.lastIndex++;
			}
			
			// The result can be accessed through the `m`-variable.
			m.forEach((match, groupIndex) => {
				values.matchs.push(match);
			});
		}	

		return values;
	}

	static scrollTop(element, delay){
		$("body, html").animate({scrollTop: element.offset().top}, delay);
	}

	static genGuid(prefix){
		var temp = "";
		var d = new Date();
		var token = Math.floor((Math.random() * 9999) + 1000);
		if (prefix) temp = prefix + "-";

		function s4() {
			return Math.floor((1 + Math.random()) * 0x10000)
				.toString(16)
				.substring(1);
		}
		var gen = temp + s4() + s4() + '_' + s4() + '_' + s4() + '_' + s4() + '_' + s4() + s4() + s4();
		return gen;

	}

	static addToList(list, toAdd, criteia){
		if(list.length == 0 ) list.push(toAdd);

		var addedElement = false;
		for(var key in list){
			var value = list[key];

			if(criteia(value, toAdd)){
				value = toAdd;
				addedElement = true;
				break;
			}
		}

		if(!addedElement){
			list.push(toAdd);
		}

		return list;
	}

	static toggleToList(list, toToggle, criteia){
		if(this.existToList(list, toToggle, criteia)){
			return this.removeToList(list, toToggle, criteia);
		}

		return this.addToList(list, toToggle, criteia);
	}

	static existToList(list, toSearch, criteia){
		if(list.length == 0 ) return false;

		for(var key in list){
			var value = list[key];

			if(criteia(value, toSearch)){
				return true
			}
		}

		return false;
	}

	static removeToList(list, toRemove, criteia){
		if(list.length == 0 ) return list;

		var keyToDelete = null;

		for(var key in list){
			var value = list[key];

			if(criteia(value, toRemove)){
				keyToDelete = key;
				break;
			}
		}

		if(keyToDelete != null){
			list.splice(keyToDelete, 1);
		}
		return list;
	}

	static each(elements, interator){
		for(var key in elements){
			var element = elements[key];

			interator(element, key);
		}
	}

	static filter(elements, compare, condition){
		var listReturn = [];
		for(var key in elements){
			var element = elements[key];

			if(condition(element, compare) != null){
				listReturn.push(element);
			}
		}

		return listReturn;
	}

	static mapping(elements, manipulation){
		var listReturn = [];
		for(var key in elements){
			var element = elements[key];

			var worked = manipulation(element, key);
			if( worked != null){
				listReturn.push(worked);
			}
		}

		return listReturn;
	}

	static search(elements, compare, condition){
		for(var key in elements){
			var element = elements[key];

			if(condition(element, compare, key)){
				return element
			}

		}
	}

	static rndRangeInt(min, max){
		return Math.floor(Math.random() * (max - min + 1)) + min;
	}

	static rndRange(min, max){
		return Math.random() * (max - min) + min;
	}

	static collectionModel(model, data){
		var returnCollection = [];
		for(var key in data){
			if (isNaN(key)) continue;
			var value = data[key];

			returnCollection.push(new model(value));
		}

		return returnCollection;
	}

    static getUrlParams () {
		var response = {};
		if (window.location.search) {
			window.location.search.substr(1).split`&`.forEach(item => {
				let [k,v] = item.split`=`; 
				v = v && decodeURIComponent(v); 
				
				response[k] = v;
			})
		}
		
		return response;
	}

	static cache(uid, invoke){
		return new Promise((resolve, reject)=>{
			let dataCached = Global.getCache('uid');

			if(dataCached == null){
				let store = (data)=>{
					localStorage.setItem(uid, JSON.stringify(data));

					resolve(data);
				}

				let fail = (err)=>{
					reject(err)
				}

				invoke(store, fail)
			}else{
				resolve(dataCached)
			}
		})
	}

	static setCache(uid, data){
		localStorage.setItem(uid, JSON.stringify(data));
	}

	static getCache(uid){
		return JSON.parse(localStorage.getItem(uid));
	}
}


