export class Model {
	constructor(data={}){
		var __model = this._model();
		for(var key in __model){
			var value = __model[key]
			this[key] = value;
		}

		this._setData(data);
	}

	_setData(data){
		var __model = this._model();
		
		for(var key in __model){
			if(typeof data[key] != 'undefined'){
				this[key] = data[key];
			}
		}
	}

	_model(){
		return {};
	}

	_validation(object, Type){
		if (!this._isValid(object, Type)) throw new TypeError("Value of argument 'Type' violates.");
	}

	_isValid(object, Type){
		return (object instanceof Type);
	}

	_getModel(){
		var toExport = {};
		for(var key in this){
			toExport[key] = this[key];
		}

		return toExport;
	}
}