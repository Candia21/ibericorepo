import { Global } from '.';

import $ from 'jquery';

export class Rest {

	static send(options = {}){
		this.url =  options.url || '';
		this.data = options.data || null
		this.method = options.method || 'GET';
		this.headers = options.headers || null;
		this.contentType = options.contentType || null

		return new Promise((resolve, reject) => {
			$.ajax({
				type: this.method,
				url: this.url, 
				data: this.data,
				headers: this.headers,
				contentType: this.contentType,
				success: (e)=>resolve(e),
				error: (e)=>reject(e)
			})
		})
	}
}
