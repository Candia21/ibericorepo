/**
 * Author: Sanchez Jean Paul
 * Date: 07/2017
 */

export class Message {

	constructor(ncode, nMessage, ntype, nStackTrace){
		this.code 				= ncode;
		this.message 			= nMessage;
		this.type 				= ntype;
		this.stackTrace 		= nStackTrace;
	}

}


/**
 * Author: Sanchez Jean Paul
 * Date: 07/2017
 */
export class Messages {

	constructor(message = null){
		this.errors = [];
		this.warnings = [];
		this.infos = [];
		this.success = [];


		if(!arguments.length) {
			return
		}else {
			this.errors = message.errors;
			this.warnings = message.warnings;
			this.infos = message.infos;
			this.success = message.success;
		}
	}

	Add(ncode, nMessage, ntype, nStackTrace = null){
		var msg = new Message(ncode, nMessage, ntype, nStackTrace);

		if(ntype == 'ERROR'){
			this.errors.push(msg);
		}else if(ntype == 'WARNING'){
			this.warnings.push(msg);
		}else if(ntype == 'INFO'){
			this.infos.push(msg);
		}else{
			this.success.push(msg);
		}
	}

	getFirstError(){
		if(!this.isSuccess()){
			return this.errors[0]
		}
	}

	getAll(types = ['error', 'warning', 'info', 'success']){
		var returnList = [];

		if(types.includes('error')){
			for(var key in this.errors){
				returnList.push(this.errors[key]);
			}	
		}

		if(types.includes('warning')){
			for(var key in this.warnings){
				returnList.push(this.warnings[key]);
			}
		}

		if(types.includes('info')){
			for(var key in this.infos){
				returnList.push(this.infos[key]);
			}
		}

		if(types.includes('success')){
			for(var key in this.success){
				returnList.push(this.success[key]);
			}
		}

		return returnList;
	}
	
	isSuccess()	  	{ return !this.hasErrors();			}
	hasErrors()	  	{ return this.errors.length > 0;	}
	hasWarnings()	{ return this.warnings.length > 0;  }
	hasInfos()		{ return this.infos.length > 0;	  	}
}
