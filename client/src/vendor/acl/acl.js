import { Service } from 'utils';

export class Acl{

	constructor(){
		this.resources = $.cookie('acl') || [];

		Service.set('Acl', this);
	}

	setResourses(resources = []){
		this.resources = resources;
		
		$.cookie('acl', this.resources, {expires : 1});
	}

	isAllowed(resource){
		return this.resources.indexOf(resource) > 0
	}

	isDeny(resource){
		return !this.isAllowed(resource);
	}
}